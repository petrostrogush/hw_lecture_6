package com.test.car;

import static org.testng.Assert.assertEquals;
import org.testng.Assert;
import org.testng.annotations.Test;

public class testForEqualizer {
	
	@Test
	public void modelColorEqual() {
		Car car = new Car("Skoda", "Silver");
		Car car2 = new Car ("Skoda", "Silver");
		
		Boolean expectedResult = true;
		Boolean actualResult = car.equals(car2);
		Assert.assertEquals(actualResult, expectedResult);
	}
	
	@Test
	public void modelNotEqual() {
		Boolean expectedResult = false;
		
		Car car = new Car("Skoda", "Silver");
		Car car2 = new Car ("Ford", "Silver");
		Boolean actualResult = car.equals(car2);
		Assert.assertEquals(actualResult, expectedResult);
	}

	@Test
	public void colorNotEqual() {
		Boolean expectedResult = false;
		
		Car car = new Car("Skoda", "Silver");
		Car car2 = new Car ("Skoda", "Red");
		Boolean actualResult = car.equals(car2);
		Assert.assertEquals(actualResult, expectedResult);
	}
	@Test
	public void colorModelNotEqual() {
		Boolean expectedResult = false;
		
		Car car = new Car("Skoda", "Silver");
		Car car2 = new Car ("Audi", "White");
		Boolean actualResult = car.equals(car2);
		Assert.assertEquals(actualResult, expectedResult);
	}
}
